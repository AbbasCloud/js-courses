function concatStrings(string, sep) {
  const isValidString = (stringToCheck) => {
    return typeof stringToCheck === "string";
  };

  const result = [string];

  const separator = isValidString(sep) ? sep : null;

  function fn(str) {
    if (!isValidString(str)) {
      return separator ? result.join(separator) : result.join("");
    }
    result.push(str);
    return fn;
  }
  return fn;
}

class Calculator {
  constructor(x, y) {
    this._x = x;
    this._y = y;
    this.logSum = this.logSum.bind(this);
    this.logMul = this.logMul.bind(this);
    this.logSub = this.logSub.bind(this);
    this.logDiv = this.logDiv.bind(this);

    this._init();
  }

  _checkError(operand) {
    if (typeof operand === "bigint") {
      throw new Error("операнд не может быть BigInt");
    }
    if (isNaN(operand)) {
      throw new Error("операнд должен быть числом");
    }
    if (operand === null) {
      throw new Error("операнд не может иметь значение null");
    }
    if (!isFinite(operand)) {
      throw new Error("операнд не может быть Infinity");
    }
  }

  _init() {
    this._checkError(this._x);
    this._checkError(this._y);
  }

  setX(newX) {
    this._checkError(newX);
    this._x = newX;
  }
  setY(newY) {
    this._checkError(newY);
    this._y = newY;
  }

  logSum() {
    console.log(this._x + this._y);
  }

  logMul() {
    console.log(this._x * this._y);
  }

  logSub() {
    console.log(this._x - this._y);
  }

  logDiv() {
    if (this._y === 0) {
      throw new Error("на ноль делить недьзя");
    }
    console.log(this._x / this._y);
  }
}

//я так понял, что дробные числа тоже валидны, т.к поддаются конвертации в другие системы исчисления наравне с целыми.

function numberSystemConverter(intToConvert, numberSystem) {
  return intToConvert.toString(numberSystem);
}

const number = new Number(prompt("введите первое число"));

const numberSystem = new Number(prompt("введите второе число"));

const errorMessage = "некоректный ввод";

if (numberSystem >= 2 && numberSystem <= 36 && !isNaN(number)) {
  const result = numberSystemConverter(number, numberSystem);
  alert(result);
} else {
  console.error(errorMessage);
}

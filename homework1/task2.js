function makeResultString(firstNumber, secondNumber) {
  return `Ответ: ${firstNumber + secondNumber}, ${firstNumber / secondNumber}.`;
}

const firstNumber = parseFloat(prompt("введите первое число"), 10);

const secondNumber = parseFloat(prompt("введите второе число"), 10);

const errorMessage = "некоректный ввод";

if (!isNaN(firstNumber) && !isNaN(secondNumber) && secondNumber != 0) {
  const result = makeResultString(firstNumber, secondNumber);
  alert(result);
} else {
  console.error(errorMessage);
}

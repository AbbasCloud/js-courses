export default class Input {
  constructor(id = "input", value = "0") {
    this._id = id;
    this._input = null;
    this._maxLength = 20;
    this.value = value;
    this._init();
  }
  _init() {
    this._input = document.querySelector(`#${this._id}`);
    this._input.value = this.value;
  }

  changeValue(newValue) {
    this._input.value = newValue;
    this.value = newValue;
  }
  setValue(newValue) {
    if (this._input.value.length > this._maxLength) return;

    if (this.value === "0") {
      this._input.value = newValue === "." ? this.value + newValue : newValue;
      this.value = newValue === "." ? this.value + newValue : newValue;
      return;
    }
    if (newValue === "." && this.value.includes(".")) {
      return;
    }
    if (this.value === "-0") {
      this._input.value =
        newValue === "." ? this.value + newValue : `-${newValue}`;

      this.value = newValue === "." ? this.value + newValue : `-${newValue}`;
      return;
    }
    this.changeValue(this.value + newValue);
  }
  getInputValue() {
    return parseFloat(this.value);
  }
}

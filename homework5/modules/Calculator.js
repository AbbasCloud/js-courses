//я не знал, что приватные свойства можно назначать # , поэтому у меня по старинке _ боюсь поламать все если переделаю))

import Input from "./Input.js";

export default class Calculator {
  constructor(container = "calculator") {
    this._container = container;
    this._inputField = new Input();
    this._operation = null;
    this._x = 0;
    this._y = 0;
    this._result = null;
    this._isXset = false;
    this._isFerstIteration = true;
    this._isStart = false;
    this._isFinished = false;
    this._isError = false;
    this._init();
  }
  //метод инициализации привязывает обработчики к кнопкам
  _init() {
    const colculator = document.querySelector(`.${this._container}`);

    colculator.addEventListener("click", (event) => {
      if (event.target.classList.contains("calculator__operand")) {
        this._setImputFieldValue(event.target);
      }

      if (event.target.classList.contains("calculator__cansel")) {
        this._cancel();
      }

      if (event.target.classList.contains("calculator__operation")) {
        this._choseOperation(event.target);
      }

      if (event.target.classList.contains("calculator__otherOperation")) {
        this._changingValueOperation(event.target.innerText);
      }

      if (event.target.classList.contains("calculator__result")) {
        this._compleetOperation();
        this._prepereNextOperation();
      }
    });
  }
  _setX(num) {
    this._x = num;
  }
  _setY(num) {
    this._y = num;
  }
  //задает инпуту новое значение не работает при ошибке и после вывода результата
  _setImputFieldValue(element) {
    if (this._isFinished) return;

    if (this._isError) return;

    this._inputField.setValue(element.innerText);
    this._isStart = true;
  }
  //выполняет операцию при нажатии на =
  _compleetOperation() {
    if (this._operation) {
      this._setY(this._inputField.getInputValue());
      this._result = this._calculation();
      this._isFinished = true;
    }
  }
  //подгатавливает приложение к дальнейщим вычислениям
  _prepereNextOperation() {
    this._inputField.changeValue(this._result);
    this._setX(this._result);
    this._setY(0);
    this._operation = null;
  }
  //происходит переназначение операции работает по разному в зависимости от состояния калькулятора
  _choseOperation(element) {
    if (this._isError) return;

    if (!this._isStart) return;

    if (this._isFinished) {
      this._operation = element.innerText;
      this._inputField.changeValue("0");
      this._isFinished = false;
      return;
    }
    if (this._isXset) {
      this._operation = element.innerText;
      this._setY(this._inputField.getInputValue());
      this._result = this._calculation();
      this._setX(this._result);
      this._inputField.changeValue("0");
      return;
    }
    if (this._operation === null) {
      this._operation = element.innerText;
      this._setX(this._inputField.getInputValue());
      this._inputField.changeValue("0");
      this._isXset = true;
      return;
    }
  }
  //сброс до зоводских)
  _cancel() {
    this._inputField.changeValue("0");
    this._setX(0);
    this._setY(0);
    this._operation = null;
    this._result = null;
    this._isFerstIteration = true;
    this._isStart = false;
    this._isError = false;
    this._isXset = false;
    this._isFinished = false;
  }

  _changeSighn() {
    if (this._inputField.value[0] != "-") {
      return "-" + this._inputField.value;
    } else {
      return this._inputField.value.slice(1);
    }
  }

  _removeLastSimnol() {
    let newValue = null;
    if (this._inputField.value[0] != "-") {
      newValue =
        this._inputField.value.length === 1
          ? "0"
          : this._inputField.value.slice(0, -1);
    } else {
      newValue =
        this._inputField.value.length === 2
          ? "-0"
          : this._inputField.value.slice(0, -1);
    }
    return newValue;
  }
  //операции изменяющие значение
  _changingValueOperation(operation) {
    if (this._isFinished) return;

    if (this._isError) return;

    let newValue = null;

    switch (operation) {
      case "+/-":
        newValue = this._changeSighn();
        break;

      case "=>":
        newValue = this._removeLastSimnol();
        break;
    }

    this._inputField.changeValue(newValue);
  }
  //вычисляет кол-во знаков после запятой
  _getFloatLength(number) {
    let length = 0;

    let num = number;

    if (num % 1 != 0) {
      do {
        num = num * 10;
        length++;
      } while (num % 1 != 0);
      return length;
    }

    return 0;
  }

  _calculation() {
    let result = null;
    switch (this._operation) {
      case "+":
        result = this._x + this._y;
        break;
      case "*":
        result = this._x * this._y;
        break;
      case "/":
        if (this._y === 0) {
          result = "на ноль делить нельзя";
          this._isError = true;
        } else {
          result = this._x / this._y;
        }
        break;
      case "-":
        result = this._x - this._y;
        break;
    }
    if (isNaN(result)) {
      return result;
    }
    if (this._getFloatLength(result) > 7) {
      return result.toFixed(8);
    }

    return result;
  }
}

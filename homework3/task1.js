Array.prototype.myFilter = function (collBack, contextObject) {
  //я сомневался стоит ли добавлять проверки this на то является ли он массивом,
  // решил что не надо т.к он не вызовется у других объектов , чтобы не перегружать код
  if (typeof collBack != "function") {
    throw new Error("колбэк должен быть функцией");
  }

  const length = this.length;

  let collBackContext = this;

  const result = [];

  if (arguments.length > 1) {
    collBackContext = contextObject;
  }

  for (let i = 0; i < length; i++) {
    if (!(i in this)) {
      continue;
    }
    if (collBack.call(collBackContext, this[i], i, this)) {
      result.push(this[i]);
    }
  }

  return result;
};

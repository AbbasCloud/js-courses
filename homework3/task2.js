function createDebounceFunction(collBack, delay) {
  if (typeof collBack != "function") {
    throw new Error("колбэк должен быть функцией");
  }

  let timeout;

  return function () {
    const colledFn = () => {
      collBack.apply(this, arguments);
    };
    clearTimeout(timeout);

    timeout = setTimeout(colledFn, delay);
  };
}

function makeObjectDeepCopy(object) {
  if (object.constructor != Object) {
    throw new Error("Данные некорректны");
  }

  const CopiedObject = {};

  for (key in object) {
    // я прочитал, что цикл for in  может перебирать свойства портотипа, поэтому добавил дополнительную проверку,
    //  но я не уверен в правильности такого решения, подскажи пожалуйста, как надо делать в таких случаях.
    if (object.hasOwnProperty(key)) {
      switch (object[key].constructor) {
        case Object:
          CopiedObject[key] = makeObjectDeepCopy(object[key]);
          break;

        case Array:
          CopiedObject[key] = object[key].map((arrElement) => arrElement);
          break;

        case Function:
          CopiedObject[key] = object[key].bind(CopiedObject);
          break;

        default:
          CopiedObject[key] = object[key];
          break;
      }
    }
  }

  return CopiedObject;
}

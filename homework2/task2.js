function selectFromInterval(array, firstIntervalValue, secondIntervalValue) {
  const checkArrayErrors = (array) => {
    if (!Array.isArray(array)) {
      return true;
    }
    return array.some((value) => typeof value != "number");
  };

  const conditionsArray =
    firstIntervalValue < secondIntervalValue
      ? [firstIntervalValue, secondIntervalValue]
      : [secondIntervalValue, firstIntervalValue];

  if (checkArrayErrors(array)) {
    throw new Error("неверное значение одного или более элементов массива");
  }

  if (checkArrayErrors(conditionsArray)) {
    throw new Error("неверное значение границ интервала");
  }

  return array.filter(
    (element) => element >= conditionsArray[0] && element <= conditionsArray[1]
  );
}
